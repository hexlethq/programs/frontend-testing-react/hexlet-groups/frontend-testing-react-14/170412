const axios = require('axios');

// BEGIN
const get = (url) => axios.get(url);

const post = (url, body) => axios.post(url, body);
// END

module.exports = { get, post };
