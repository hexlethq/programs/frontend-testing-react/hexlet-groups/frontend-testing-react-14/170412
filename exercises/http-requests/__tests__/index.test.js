const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');

// BEGIN
const basePath = 'http://example.com';
const uri = '/users';
const url = `${basePath}${uri}`;
const testBody = { test: 'test' };
nock.disableNetConnect();

describe('http-requests', () => {
  it('get', async () => {
    const scope = nock(basePath)
      .get(uri)
      .reply(200, testBody);
    const { data } = await get(url);
    scope.isDone();
    expect(data).toEqual(testBody);
  });

  it('post', async () => {
    const scope = nock(basePath)
      .post(uri, testBody)
      .reply(200, testBody);
    const { data } = await post(url, testBody);
    scope.isDone();
    expect(data).toEqual(testBody);
  });
});
// END
