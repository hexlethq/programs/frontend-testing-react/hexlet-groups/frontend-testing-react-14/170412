// BEGIN
const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');
require('expect-puppeteer');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  it('Главная страница приложения открывается', async () => {
    await page.goto(appUrl);

    await expect(page).toMatch('Simple Blog');

    await expect(page).toMatch('Welcome to a Simple blog!');
  });

  it('Список статей', async () => {
    await page.goto(appUrl);
    await expect(page).toClick('[data-testid="nav-articles-index-link"]');
    await page.waitForSelector('[data-testid="articles-table"]');
  });

  it('Создание, редактирование и удаление статьи', async () => {
    await page.goto(appArticlesUrl);

    await expect(page).toClick('[data-testid="article-create-link"]');
    await page.waitForSelector('form[action="/articles"]');

    const articleTitle = 'Test';
    const articleContent = 'Lorem ipsum';
    await expect(page).toFillForm('form[action="/articles"]', {
      'article[name]': articleTitle,
      'article[content]': articleContent,
    });

    await expect(page).toSelect('[name="article[categoryId]"]', '1');
    await expect(page).toClick('[data-testid="article-create-button"]');

    await page.waitForSelector('[data-testid="articles-table"]');

    await expect(page).toMatch(articleTitle);
    const createdArticle = await expect(page).toMatchElement('tr', { text: articleTitle });
    const createdArticleId = await createdArticle.$eval('td', (element) => element.textContent);
    await expect(page).toClick(`[data-testid="article-edit-link-${createdArticleId}"]`);
    await page.waitForSelector(`form[action="/articles/${createdArticleId}"]`);

    const updatedArticleTitle = 'Text';
    await expect(page).toFillForm(`form[action="/articles/${createdArticleId}"]`, {
      'article[name]': updatedArticleTitle,
    });

    await expect(page).toClick('[data-testid="article-update-button"]');

    await page.waitForSelector('[data-testid="articles-table"]');
    await expect(page).not.toMatch(articleTitle);
    await expect(page).toMatch(updatedArticleTitle);

    await expect(page).toClick(`[data-testid="article-delete-link-${createdArticleId}"]`);
    await page.waitForNavigation();
    await expect(page).not.toMatch(updatedArticleTitle);
  });

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});

// END
