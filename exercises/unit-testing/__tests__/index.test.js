test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  // BEGIN
  const expected = { a: 'a', b: 'b', k: 'v' };
  expect(result).toEqual(expected);

  expect(target).toEqual(expected);
  // END
});

test('throw error when first param null or undefined', () => {
  expect(() => Object.assign(undefined, {})).toThrow();
  expect(() => Object.assign(null, {})).toThrow();
});

test('throw error when target object property readonly', () => {
  const target = Object.defineProperty({}, 'foo', {
    value: 1,
    writable: false,
  });
  const src = { foo: 2 };

  expect(() => Object.assign(target, src)).toThrow();
});

test('return target object', () => {
  const target = { k: 'v2', a: 'a' };
  const src = { k: 'v', b: 'b' };
  const result = Object.assign(target, src);

  expect(result).toBe(target);
});
