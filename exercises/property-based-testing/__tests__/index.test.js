const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('properties', () => {
  it('сортировка не изменяет количество элементов и содержит все элементы исходного массива', () => {
    const count = (elements, countedElement) => elements
      .filter((element) => element === countedElement).length;
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted).toHaveLength(data.length);
        data.forEach((item) => {
          expect(count(sorted, item)).toBe(count(sorted, item));
        });
      }),
    );
  });

  it('возвращает отсортированный массив', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted).toBeSorted();
      }),
    );
  });

  it('сортировка не сортирует остортированный массив', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted).toEqual(sort(sorted));
      }),
    );
  });
});

// END
