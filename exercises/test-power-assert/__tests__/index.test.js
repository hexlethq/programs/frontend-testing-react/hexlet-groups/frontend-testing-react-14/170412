const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN

assert.deepStrictEqual(flattenDepth([]), [], 'empty array');

assert.deepStrictEqual(flattenDepth([[]]), [], 'nested empty array');

assert.deepStrictEqual(flattenDepth([1, [2, [3, [4]]]]), [1, 2, [3, [4]]], 'default depth 1');

assert.deepStrictEqual(flattenDepth([1, [2, [3, [4]]]], 2), [1, 2, 3, [4]], 'depth 2');

assert.deepStrictEqual(flattenDepth([1, [2, [3, [4]]]], -2), [1, [2, [3, [4]]]], 'depth -2');

// END
