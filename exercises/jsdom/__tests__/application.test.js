// @ts-check

require('@testing-library/jest-dom');

const fs = require('fs');
const path = require('path');

const run = require('../src/application');

beforeEach(() => {
  const initHtml = fs
    .readFileSync(path.join('__fixtures__', 'index.html'))
    .toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
const getBySelector = (selector, parentElement = document) => parentElement
  .querySelector(selector);
const getAllBySelector = (selector, parentElement = document) => parentElement
  .querySelectorAll(selector);

describe('todo app positive cases', () => {
  it('initial state', () => {
    const lists = getBySelector('[data-container="lists"]');
    expect(lists).toHaveTextContent('General');

    const tasks = getBySelector('[data-container="tasks"]');
    expect(tasks).toBeEmptyDOMElement();
  });

  it('add new list & tasks', () => {
    const addListInput = getBySelector('[data-testid="add-list-input"]');
    addListInput.value = 'new list';

    const submitListBtn = getBySelector('[data-testid="add-list-button"]');
    submitListBtn.click();

    expect(addListInput).toHaveDisplayValue('');

    const addTaskInput = getBySelector('[data-testid="add-task-input"]');
    addTaskInput.value = 'first task';

    const submitTaskBtn = getBySelector('[data-testid="add-task-button"]');
    submitTaskBtn.click();

    expect(addTaskInput).toHaveDisplayValue('');

    const tasks = getBySelector('[data-container="tasks"]');

    expect(getAllBySelector('li', tasks)).toHaveLength(1);
    expect(tasks).toHaveTextContent('first task');

    addTaskInput.value = 'second task';
    submitTaskBtn.click();

    expect(getAllBySelector('li', tasks)).toHaveLength(2);
    expect(tasks).toHaveTextContent('second task');

    const newListItem = getBySelector('[data-testid="new list-list-item"]');
    newListItem.click();

    expect(tasks).toBeEmptyDOMElement();
    addTaskInput.value = 'third task';
    submitTaskBtn.click();
    expect(tasks).toHaveTextContent('third task');
  });
});
// END
