const faker = require('faker');

// BEGIN
describe('createTransaction test', () => {
  it('createTransaction() соответсвует паттерну', () => {
    const transaction = faker.helpers.createTransaction();
    expect(transaction).toEqual({
      amount: expect.stringMatching(/^[+-]?\d+(\.\d{2})?$/),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.stringMatching(/^(\d{8})?$/),
    });
  });

  it('createTransaction не детерменирован', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();
    expect(transaction1).not.toEqual(transaction2);
  });
});
// END
