const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  it('Главная страница приложения открывается', async () => {
    await page.goto(appUrl);
    const title = await page.title();
    expect(title).toBe('Simple Blog');
    const mainSectionTitle = await page.$eval('.card #title', (element) => element.textContent);
    expect(mainSectionTitle).toBe('Welcome to a Simple blog!');
  });

  it('Список статей', async () => {
    await page.goto(appUrl);
    const articlesLink = await page.$('a[href="/articles"]');
    articlesLink.click();
    const articlesTable = await page.waitForSelector('table#articles');
    const articles = await articlesTable.$$('.tr');
    expect(articles).toBeTruthy();
  });

  it('Создание, редактирование и удаление статьи', async () => {
    await page.goto(appArticlesUrl);
    const createArticleLink = await page.$('a[href="/articles/new"]');
    createArticleLink.click();
    await page.waitForSelector('form[action="/articles"]');
    const articleTitle = 'Test';
    const articleContent = 'Lorem ipsum';
    await page.type('[name="article[name]"]', articleTitle);
    await page.select('[name="article[categoryId]"]', '1');
    await page.type('[name="article[content]"]', articleContent);
    await page.click('[type="submit"]');
    const articlesTable = await page.waitForSelector('table#articles');
    const articlesRows = await page.$$('tr');

    const [createdArticle] = await articlesTable.$x(`//tr/td[contains(text(), '${articleTitle}')]/parent::tr`);

    const editLink = await createdArticle.$('[href*="edit"]');
    editLink.click();
    await page.waitForSelector('#edit-form');
    const newArticleTitle = 'Title1';
    await page.type('[name="article[name]"]', newArticleTitle);
    await page.click('[type="submit"]');
    const updatedArticlesTable = await page.waitForSelector('table#articles');

    const [editedArticle] = await updatedArticlesTable.$x(`//tr/td[contains(text(), '${newArticleTitle}')]/parent::tr`);

    const deleteBtn = await editedArticle.$('[value="Delete"]');
    await Promise.all([
      deleteBtn.click(),
      page.waitForNavigation(),
    ]);
    const articlesRowsAfterDelete = await page.$$('tr');
    expect(articlesRowsAfterDelete).toHaveLength(articlesRows.length - 1);
  });
  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
